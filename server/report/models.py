from django.db import models


class Document(models.Model):
    name = models.CharField(primary_key=True, max_length=1000)

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.__repr__()


class WordUsage(models.Model):
    document = models.ForeignKey(Document, max_length=1000, on_delete=models.CASCADE)
    word = models.CharField(max_length=1000)
    count = models.IntegerField()

    class Meta:
        unique_together = ('document', 'word')
        ordering = ('-count',)

    def __eq__(self, other):
        return self.document == other.document and self.word == other.word

    def __hash__(self):
        return hash((self.document, self.word))

    def __repr__(self):
        return f'{self.document} - {self.word}: {self.count}'

    def __str__(self):
        return self.__repr__()


class Sentence(models.Model):
    value = models.CharField(max_length=1000)
    word = models.CharField(max_length=1000)

    class Meta:
        unique_together = ('value', 'word')

    def __repr__(self):
        return self.value

    def __str__(self):
        return self.__repr__()

    def __eq__(self, other):
        return self.value == other.value and self.word == other.word

    def __hash__(self):
        return hash((self.value, self.word))
