from django.core.management.base import BaseCommand, CommandError
import logging
import os

from import_strategy.import_strategy import ImportStrategyFactory, ImportStrategyType
from output_strategy.output_strategy import OutputStrategyFactory, OutputStrategyType
from services.tag_service import tag


logger = logging.getLogger(__name__)


class Command(BaseCommand):

    help = 'Creates tags for document'

    def add_arguments(self, parser):
        parser.add_argument(
            '--source_type',
            type=str,
            help='Type of source document (i.e. txt, csv, sql). Default is txt',
            default=ImportStrategyType.TEXT_DOCUMENT
        )

        parser.add_argument(
            '--output_type',
            type=str,
            help='Type of output document (i.e. txt, csv, sql). Default is csv',
            default=OutputStrategyType.CSV_DOCUMENT
        )

        parser.add_argument(
            '--source_paths',
            type=str,
            help='Path to source document',
            nargs='+'
        )

        parser.add_argument(
            '--output_dir',
            type=str,
            help='Path to directory where output files should be places',
        )

        parser.add_argument(
            '--source_dir',
            type=str,
            help='Directory with documents',
        )

        parser.add_argument(
            '--async',
            help="Runs files analyse in async mode. It's still under development",
            default='NO',
            nargs='?',
            choices=['YES', 'NO']
        )

    def handle(self, *args, **options):
        source_dir = options['source_dir']
        source_type = options['source_type']
        source_paths = options['source_paths']
        output_type = options['output_type']
        output_dir = options['output_dir']

        if source_paths is None and source_dir is None:
            raise CommandError('You must specify either source_paths or source_dir')

        if source_paths is not None:
            logger.warning("Getting files listed in source_paths.")

        if source_dir is not None:
            source_paths = (os.path.join(source_dir, file_name) for file_name in os.listdir(source_dir))

        if options['async'] == 'YES':
            logger.warning("async option is not supported yet. It'll be omitted.")

        for source_path in source_paths:
            tag(
                os.path.basename(source_path),
                list(ImportStrategyFactory(source_path=source_path).get(source_type).data())
            )

        OutputStrategyFactory(output_dir=output_dir).get(strategy_type=output_type).load()
