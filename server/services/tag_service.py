from collections import defaultdict

from report.models import Document, Sentence, WordUsage
from server.celery import app


def filter_not_word(word):
    return word.isalpha() or all(sub_word.isalpha() for sub_word in word.split("'"))


@app.task()
def tag(document_name, data):
    WordUsage.objects.filter(document=document_name).delete()
    document, _ = Document.objects.get_or_create(name=document_name)

    documents_words = set()
    document_sentences = set(Sentence.objects.all())
    Sentence.objects.all().delete()
    document_data = defaultdict(int)
    for row in data:
        for word in filter(lambda w: filter_not_word(w), row.split(" ")):
            cleaned_word = word.replace(',', '')
            document_data[cleaned_word] += 1
            document_sentences.add(Sentence(value=row, word=word))

    for word, word_count in document_data.items():
        documents_words.add(WordUsage(document=document, word=word, count=word_count))

    WordUsage.objects.bulk_create(documents_words)
    Sentence.objects.bulk_create(document_sentences)
