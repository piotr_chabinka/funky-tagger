import abc
from collections import defaultdict
import csv
import datetime
from django.conf import settings
from enum import Enum
import logging
import re
import os

from report.models import Document, Sentence


logger = logging.getLogger(__name__)


class OutputStrategy:

    def __init__(self, output_dir=None):
        self._output_dir = output_dir or self.default_output_dir

    @staticmethod
    def _retrieve_data():
        documents = Document.objects.prefetch_related('wordusage_set').all()

        most_common_words = defaultdict(lambda: defaultdict(set))
        for document in documents:
            most_common_word = document.wordusage_set.first().word
            sentences = Sentence.objects.filter(word=most_common_word)
            most_common_words[most_common_word]['documents'].add(document.name)
            most_common_words[most_common_word]['sentences'].update(sentences)

        return most_common_words

    @property
    @abc.abstractmethod
    def default_output_dir(self):
        pass

    @abc.abstractmethod
    def load(self):
        pass


class CsvOutputStrategy(OutputStrategy):

    OUTPUT_DIR = 'output_files'
    REPORT_NAME_TEMPLATE = 'report-{}.csv'
    HEADER = ['Word', 'Documents', 'Sentences containing the word']

    @property
    def default_output_dir(self):
        return os.path.join(settings.BASE_DIR, self.OUTPUT_DIR)

    def load(self):
        if not os.path.isdir(self._output_dir):
            os.mkdir(self._output_dir)

        pretty_now = re.sub('[ :.]', '-', str(datetime.datetime.now()))
        output_path = os.path.join(
            self._output_dir,
            self.REPORT_NAME_TEMPLATE.format(pretty_now)
        )
        with open(output_path, 'w', newline='') as output_file:
            tag_writer = csv.writer(output_file, delimiter=';')
            tag_writer.writerow(self.HEADER)
            for word, word_data in self._retrieve_data().items():
                tag_writer.writerow(
                    (word, word_data['documents'], word_data['sentences'])
                )


class OutputStrategyType(Enum):
    CSV_DOCUMENT = 'csv'


class OutputStrategyFactory:

    instance = None

    strategies = {
        OutputStrategyType.CSV_DOCUMENT: CsvOutputStrategy,
        OutputStrategyType.CSV_DOCUMENT.value: CsvOutputStrategy
    }

    def __new__(cls, *args, **kwargs):
        if OutputStrategyFactory.instance is None:
            OutputStrategyFactory.instance = object.__new__(cls)
        return OutputStrategyFactory.instance

    def get(self, strategy_type):
        try:
            return self.strategies[strategy_type]()
        except KeyError:
            logger.warning(f'Not found strategy {strategy_type}.')
