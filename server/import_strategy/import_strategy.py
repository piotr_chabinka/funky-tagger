import abc
from enum import Enum
import logging


logger = logging.getLogger(__name__)


class ImportStrategyType(Enum):
    TEXT_DOCUMENT = 'txt'


class ImportStrategy:

    def __init__(self, source_path):
        self._source_path = source_path

    @abc.abstractmethod
    def data(self):
        pass


class TextDocumentImportStrategy(ImportStrategy):

    @staticmethod
    def split_line(line):
        for sentence in line.split('.'):
            if sentence and not sentence.isspace():
                yield sentence

    def data(self):
        with open(self._source_path, 'r') as source:
            for line in source:
                yield from self.split_line(line)


class ImportStrategyFactory:

    instance = None

    strategies = {
        ImportStrategyType.TEXT_DOCUMENT: TextDocumentImportStrategy
    }

    def __new__(cls, *args, **kwargs):
        if ImportStrategyFactory.instance is None:
            ImportStrategyFactory.instance = object.__new__(cls)
        return ImportStrategyFactory.instance

    def __init__(self, source_path):
        self._source_path = source_path

    def get(self, strategy_type):
        try:
            return self.strategies[strategy_type](source_path=self._source_path)
        except KeyError:
            logger.warning(f'Not found strategy {strategy_type}.')
