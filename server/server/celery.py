import os
from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'server.settings')

app = Celery('funky-tagger', broker='redis://localhost:6379/0', include=['report.management.commands.tag'])
