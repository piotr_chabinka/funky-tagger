import os

from server.import_strategy.import_strategy import ImportStrategyFactory, TextDocumentImportStrategy


def test_import_strategy_factory_singleton():
    instance = ImportStrategyFactory(source_path='')

    assert instance is ImportStrategyFactory(source_path='')


def test_text_document_import_strategy():
    test_data_dir = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        'test_data'
    )
    import_strategy = TextDocumentImportStrategy(source_path=os.path.join(test_data_dir, 'import_data_test.txt'))

    expected_data = [
        'Very simple',
        'Very much so',
        ' Yup'
    ]
    assert expected_data == list(import_strategy.data())
