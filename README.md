# Funky tagger

Funky tagger is simply Django-based platform for creating hashtags in documents.

### Dependencies
To run Funky tagger you'll need to install first:
- git
- python 3.7 with pip
- sqlite3

### Usage

To run locally:

- Clone repository
```sh
$ git clone git@gitlab.com:piotr_chabinka/funky-tagger.git
$ cd funky-tagger
```
- Install dependencies
```sh
$ pip install requirements.txt
```
- Migrate database
```sh
$ python server/manage.py migrate
```
- Run tagging command
```sh
$ python server/manage.py tag
```

Run command help to see all options
```sh
$ python server/manage.py tag --help
```

### Todos
 - Full support for Docker based version
 - Running in async mode with Celery and Redis
 - Web interface for upload and viewing reports